// components/UpImg/UpImg.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
     // 声明组件要接收的数据类型
    src:{
      type:String,
      value:""
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
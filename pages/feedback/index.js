// pages/feedback/index.js
// 一、点击+号按钮的时候，触发点击事件，调用小程序内置的选择图片的API
// 获取到选中的图片路径（数组格式）
// 把图片路径存入到data变量中
// 页面就可以根据图片数组进行显示了
// 二、点击自定义组件的时候，删除当前被点击的图片
// 首先是获取被点击元素的索引
// 获取data中的图片数组
// 根据索引，在数组中删除该图片
// 把数组重新设置回data中
// 三、提交按钮的事件，用户点击提交按钮后
// 获取文本域的内容，进行合法性验证，验证不通过就弹窗提示
//     要先定义一个变量接受文本域内数据，同时给文本域绑定一个输入响应事件
// 验证通过后，就把用户选择的图片上传到专门的图片保存服务器中，然后服务器会返回图片的带url的地址
//      遍历存储图片的数组挨个上传
//      自己在定义一个变量存储返回的图片url地址
// 文本域内容和返回的图片url地址提交到后台服务器中
// 提交成功后，清空当前页面，返回上一页。
Page({
  data: {
    // 引入自定义组件，设置初始数据
    tabs: [{
        id: 0,
        value: "体验问题",
        isActive: true
      },
      {
        id: 1,
        value: "商品、商家投诉",
        isActive: false
      }
    ],
    // 被选中的图片的路径数组
    chooseImgs: [],
    // 接收文本域存储的变量
    textVal: [],
  },
  // 图片储服务器返回的图片的url地址
  UpLoadImgs: [],
  // 顶部导航栏的点击事件
  handleTabsItemChange(e) {
    // 1 获取被点击的标题索引
    const {
      index
    } = e.detail;
    // 2 修改源数组
    let {
      tabs
    } = this.data;
    tabs.forEach((v, i) => i === index ? v.isActive = true : v.isActive = false);
    // 3 赋值到data中
    this.setData({
      tabs
    })
  },
  // 点击+号选择图片事件
  handleChooseImg(e) {
    // 调用内置的API
    wx.chooseImage({
      // 同时选择的图片数量
      count: 9,
      // 图片的格式
      sizeType: ['original', 'compressed'],
      // 图片的来源，相册和照相机
      sourceType: ['album', 'camera'],
      success: (result) => {
        // 打印查看成功后的内容，在result.tempfilepathsZ中存储着我们的图片路径
        // console.log(result);
        this.setData({
          // 图片数组进行拼接，可以使得多次选中图片，旧图片和新图片的数组拼接
          // this.data.chooseImgs是当前存储的旧图片的数组
          chooseImgs: [...this.data.chooseImgs, ...result.tempFilePaths]
        })
      }
    })
  },
  // 点击自定义组件删除图片事件
  handleRemoveImg(e) {
    // 获取被点击组件的索引
    // console.log(e);
    const {
      index
    } = e.currentTarget.dataset.index;
    // 获取data中的图片数组
    let {
      chooseImgs
    } = this.data;
    // 删除元素,要删除的元素的索引和数量
    chooseImgs.splice(index, 1)
    // 填回data中
    this.setData({
      chooseImgs
    })
  },
  // 文本域的输入响应事件
  handleTextInput(e) {
    // console.log(e);
    this.setData({
      textVal: e.detail.value
    })
  },
  // 提交按钮的响应事件
  handleFormSubmit(e) {
    // 获取文本域内容和此时的图片数组
    const {
      textVal,
      chooseImgs
    } = this.data;
    // 验证合法性
    // trim是从字符串中删除开头和结尾的空格和行终止符
    // textVal删除后如果为空，再使用！取反，则执行if中的不合法提示
    var str=textVal.trim;
    if (!str) {
      // 不合法
      wx.showToast({
        title: '您的输入为空！',
        mask: 'true',
        icon: 'error'
      })
      return;
    };
    // 上传图片到专门的图片服务器，使用小程序内置API
    // 上传文件的api不支持多个文件同时上传，所以要遍历存储图片的数组挨个上传
    // 显示正在等待的图片
    wx.showLoading({
      title: "正在上传中",
      mask: true
    });
    // 判断有没有需要上传的图片数组
    if(chooseImgs.length != 0){
      chooseImgs.forEach((v, i) => {
        wx.uploadFile({
          // 要上传的文件路径
          filePath: v,
          // 这里name的名称是和后端沟通确定的
          name: 'image',
          // 图片要上传到哪里
          url: 'https://img.coolcr.cn/api/upload',
          // 顺带的文本内容
          formData: {},
          success: (result) => {
            let url = JSON.parse(result.data).data.url;
            this.UpLoadImgs.push(url);
            // 所有图片都上传完毕才触发的if
            if (i === chooseImgs.length - 1) {
              // 关闭弹窗
              wx.hideLoading();
              // 这里使用log输出 代替我们向后端提交数据
              console.log("把文本的内容和外网的图片数组 提交到后台中");
              // 提交后，重置本页面
              this.setData({
                textVal: "",
                chooseImgs: []
              })
              // 返回上一页
              wx.navigateBack({
                delta: 1
              });
            }
          }
        })
      })}else(
        // 若没有图片上传，只有文本，则执行如下
        wx.hideLoading(),
        console.log('只提交了文本'),
        wx.navigateBack({
          delta: 1
        })
      )
    }
})
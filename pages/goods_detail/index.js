// pages/goods_detail/index.js
//引入用来发送请求的方法，优化后的
import {
  request
} from "../../request/index.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    //请求返回的数据是以对象形式
    goodsObj: {},
    // 商品是否被收藏
    isCollect: false

  },
  //定义要预览的大图信息数组全局变量
  goodsInfo: {},
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //拿到跳转页面时传递过来的商品id
    const {
      goods_id
    } = options;
    this.getGoodsDetail(goods_id);
  },
  /**
   * 获取商品详情数据
   */
  async getGoodsDetail(goods_id) {
    const goodsObj = await request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/goods/detail",
      data: {
        goods_id
      }
    });
    //请求成功之后给之前定义的预览大图数组赋值
    this.goodsInfo = goodsObj.data.message;
    // 1 获取缓存中的商品收藏的数组
    let collect = wx.getStorageSync("collect") || [];
    // 2 判断当前商品是否被收藏
    let isCollect = collect.some(v => v.goods_id === this.goodsInfo.goods_id);
    this.setData({
      // 优化存储数据，只赋值存储小程序用到的数据
      goodsObj: {
        goods_name: goodsObj.data.message.goods_name,
        goods_price: goodsObj.data.message.goods_price,
        // iphone部分手机不支持webp格式
        // 后台修改
        // 或者自己临时修改 使用replace函数 其中\.webp是找到所有.webp的文件，g表示全选，.jpg表示全部替换为.jpg格式。
        goods_introduce: goodsObj.data.message.goods_introduce.replace(/\.webp/g, '.jpg'),
        pics: goodsObj.data.message.pics
      },
      isCollect
    })
  },
  /**
   * 点击轮播图预览大图事件
   */
  handlePrevewImage(e) {
    console.log('预览');
    //  先构建要预览的图片数组
    const urls = this.goodsInfo.pics.map(v => v.pics_mid)
    // 接受传递过来的图片url
    const current = e.currentTarget.dataset.url
    wx.previewImage({
      current: current,
      urls: urls
    })
  },
  /**
   * 用户商品加入购物车事件
   */
  handleCartAdd(e) {
    //获取缓存的商品数据，并由字符串格式转为数组格式
    let cart = wx.getStorageSync('cart') || [];
    // 判定商品是否已存在购物车数组中
    let index = cart.findIndex(v => v.goods_id === this.goodsInfo.goods_id);
    if (index === -1) {
      //不存在，第一次添加
      this.goodsInfo.num = 1;
      // 给商品增添一个checked的属性，值为true，以便在购物车页面的复选框进行选中
      this.goodsInfo.checked = true;
      cart.push(this.goodsInfo);
    } else {
      //存在
      cart[index].num++;
    }
    //购物车数组更新到缓存中
    wx.setStorageSync("cart", cart);
    //弹窗提示
    wx.showToast({
      title: '添加成功',
      icon: 'success',
      mask: 'true'
    })
  },
  // 点击商品收藏图标事件
  handleCollect() {
    //获取用户登录信息，如果用户登录了才能执行收藏图标事件
    let userinfo = wx.getStorageSync("userinfo");
    if (userinfo) {
      let isCollect = false;
      // 1 获取缓存中的商品收藏数组
      let collect = wx.getStorageSync("collect") || [];
      // 2 判断该商品是否被收藏过
      let index = collect.findIndex(v => v.goods_id === this.goodsInfo.goods_id);
      // 3 当index！=-1表示 已经收藏过 
      if (index !== -1) {
        // 能找到 已经收藏过了  在数组中删除该商品
        collect.splice(index, 1);
        isCollect = false;
        wx.showToast({
          title: '取消成功',
          icon: 'success',
          mask: true
        });
      } else {
        // 没有收藏过
        collect.push(this.goodsInfo);
        isCollect = true;
        wx.showToast({
          title: '收藏成功',
          icon: 'success',
          mask: true
        });
      }
      // 4 把数组存入到缓存中
      wx.setStorageSync("collect", collect);
      // 5 修改data中的属性  isCollect
      this.setData({
        isCollect
      })
    }else{
      wx.navigateTo({
        url: '/pages/login/index',
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    let options = currentPage.options;
    const {
      goods_id
    } = options;
    this.getGoodsDetail(goods_id);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
})
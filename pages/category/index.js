// pages/category/index.js

//引入用来发送请求的方法，优化后的
import {
  request
} from "../../request/index.js"


Page({

  /**
   * 页面的初始数据
   */
  data: {
    //左侧的菜单数据
    leftMenuList: [],
    //右侧的菜单数据
    rightMenuList: [],
    //被点击的左侧的菜单
    currentIndex: 0,
    //右侧内容滚动条距离顶部距离
    scrolltop:0
  },
  //接口的返回数据
  Cates: [],

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 1先判断一下本地存储中有没有旧的数据，形式是｛时间戳，数据｝的形式
    // 2没有旧数据直接发送新请求
    // 3有旧的数据同时旧的数据也没有过期就使用本地存储中的旧数据即可

    //获取本地存储的数据，小程序中也是有存在本地的技术。
    const Cates = wx.getStorageSync('cates');
    //判断
    if (!Cates) {
      //不存在 发送请求获取数据
      this.getCates();
    }else{
      // 有旧的数据 定义过期时间为10秒
      if(Date.now()-Cates.time>10000){
        this.getCates();
      }else{
        this.Cates=Cates.data;
        // console.log(Cates);
        // 构造左侧的大菜单数据
        let leftMenuList = this.Cates.map(v => v.cat_name);
        // 构造右侧的商品数据
        let rightMenuList = this.Cates[0].children;
        this.setData({
          leftMenuList,
          rightMenuList
        })
       
      }
    }

  },
  // 获取分类数据
  getCates() {
    request({
        url: "https://api-hmugo-web.itheima.net/api/public/v1/categories"
      })
      .then(res => {
        this.Cates = res.data.message;
        // 把接口的数据存入到本地存储中
        wx.setStorageSync("cates", {
          time: Date.now(),
          data: this.Cates
        });
        // console.log(res);
        // 构造左侧的大菜单数据
        let leftMenuList = this.Cates.map(v => v.cat_name);
        // 构造右侧的商品数据
        let rightMenuList = this.Cates[0].children;
        this.setData({
          leftMenuList,
          rightMenuList
        })
      })
  },
  //左侧菜单的点击事件
  handleItemTap(e) {
    // console.log(e);
    // 1 获取被点击的标题身上的索引
    const {
      index
    } = e.currentTarget.dataset
    //2 给data中的currentIndex赋值就可以了
    // 3 构造此时右侧的商品数据
    let rightMenuList = this.Cates[index].children;
    this.setData({
      currentIndex: index,
      rightMenuList,
       //重新设置右侧内容的scrolltop标签的距离顶部距离
       scrolltop:0
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
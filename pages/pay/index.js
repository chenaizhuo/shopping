// pages/pay/index.js
import {
  requestPayment
} from "../../utils/util.js";
// 引入封装好的请求函数
import {
  request
} from "../../request/index.js";
Page({
  // 封装一个函数吊起微信支付接口
  /**
   * 封装一个函数，设置商品状态的同时，底部工具栏，总价格，总数量重新计算 
   */
  setCart(cart) {
    let allChecked = true;
    //  声明总价格和总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    })
    //判断数组是否为空
    allChecked = cart.length != 0 ? allChecked : false;
    this.setData({
      cart,
      totalPrice,
      totalNum,
    });
    wx.setStorageSync('cart', cart);
  },

  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    //声明一个变量，购物车数组
    cart: [],
    // 声明一个变量，购物车内容是否全选
    allChecked: false,
    // 定义商品总价格和总数量
    totalPrice: 0,
    totalNum: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //获取缓存中的购物车数据
    let cart = wx.getStorageSync('cart')
    //获取缓存中的收货地址信息
    const address = wx.getStorageSync('address');
    //过滤后的购物车数组（真正被选中的商品）
    cart = cart.filter(v => v.checked);
    //  声明总价格和总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      totalPrice += v.num * v.goods_price;
      totalNum += v.num;
    })
    this.setData({
      cart,
      totalPrice,
      totalNum,
      address,
    });
  },
  // 点击支付事件
  async handleOrderPay(e) {
    //判断缓存中有无token
    const token = wx.getStorageSync('token');
    //如果不存在
    if (!token) {
      //跳转页面到获取用户登录信息
      wx.navigateTo({
        url: '/pages/auth/index',
      })
      return;
    };
    //否则就是有token值，创建订单
    // console.log('已经有token了');
    // 准备一些必须的请求头参数
    // const header = {
    //   Authorization: wx.getStorageSync('token')
    // };
    //准备请求体参数
    const order_price = this.data.totalPrice;
    const consignee_addr = this.data.address.all;
    const cart = this.data.cart;
    let goods = [];
    cart.forEach(v => goods.push({
      goods_id: v.goods_id,
      goods_number: v.num,
      goods_price: v.goods_price
    }))
    const orderParams = {
      order_price,
      consignee_addr,
      goods
    }
    // 发送请求 创建订单，获取订单编号,这个接口,不稳定，所以返回的订单编号可能为空或者其他
    const
      number = await request({
        url: "https://api-hmugo-web.itheima.net/api/public/v1/my/orders/create",
        method: "POST",
        data: orderParams,

      });
    //把返回数据中的订单编号存入缓存中，并命名为order_number
    wx.setStorageSync('order_number', number.data.message.order_number);
    // 从缓存中取出
    const order_number = wx.getStorageSync('order_number');
    // 发起预支付接口
    const zhifu = await request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/my/orders/req_unifiedorder",
      method: "POST",
      data: {
        order_number
      },
    });
    console.log(zhifu);
    wx.setStorageSync('pay', zhifu.data.message.pay);
    const {
      pay
    } = wx.getStorageSync('pay')||{};
    //调用小程序内置API，吊起支付窗口,这里我们是把此操作进行了一个封装函数操作
    await requestPayment(pay);

    // 查询后台订单状态
    await request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/my/orders/chkOrder",
      method: "POST",
      data: {
        order_number
      }
    });
    wx.showToast({
      title: '订单支付成功',
      icon: 'success',
      duration: 2000,
    });
    //支付成功后跳转到订单页面
    wx.navigateTo({
      url: '/pages/order/index',
    })
    //  手动删除缓存中 已经支付了的商品
    let newCart = wx.getStorageSync("cart");
    newCart = newCart.filter(v => !v.checked);
    wx.setStorageSync("cart", newCart);

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
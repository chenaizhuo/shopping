  //发送ajax异步请求的初始次数
 let ajaxTimes = 0;
// params是参数
export const request = (params) => {
  //判断url中是否带有 /my/请求的是私有的路径，带上header token
  let header={...params.header};
  if(params.url.includes("/my/")){
    //拼接header
    header["Authorization"]=wx.getStorageSync('token')
  }
  //每发一次请求就自加
  ajaxTimes++;
  //显示加载中效果
  wx.showLoading({
    title: '页面加载中',
    mask: true
  })
  return new Promise((
    // resolve是请求数据成功的结果，reject是请求失败
    resolve,
    reject
  ) => {
    wx.request({
      // ...params是解构参数
      ...params,
      header:header,
      success: (result) => {
        resolve(result)
      },
      fail: (err) => {
        reject(err);
      },
      //无论成功还是失败都会触发的事件
      complete: () => {
        //每发送完一次请求，就自减一，当为0时，表明请求完毕，关闭加载中提示
        ajaxTimes--;
        if (ajaxTimes === 0) {
          //关闭页面加载中图标
          wx.hideLoading()
        }
      }
    });
  })
}
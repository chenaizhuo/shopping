// pages/cart/index.js
Page({
  /**
   * 封装一个函数，设置购物车商品状态的同时，底部工具栏，总价格，总数量重新计算 
   */
  setCart(cart) {
    let allChecked = true;
    //  声明总价格和总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      } else {
        allChecked = false;
      }
    })
    //判断数组是否为空
    allChecked = cart.length != 0 ? allChecked : false;
    this.setData({
      cart,
      totalPrice,
      totalNum,
      allChecked
    });
    wx.setStorageSync('cart', cart);
  },
  // 点击获取收货地址事件
  // 1.绑定点击事件
  // 2.调用小程序内API,"wx.chooseAddress"，获取用户的收货地址
  // 购物车页面加载完毕
  // 1.获取本地存储中的收货地址数据
  // 2.如果有存储，就把数据 设置给data中的一个变量
  handleChooseAddress(e) {
    wx.chooseAddress({
      success: (result) => {
        // 把获取的地址存入本地缓存中,在存储之前先进行一个地址拼接
        result.all = result.provinceName + result.cityName + result.countyName + result.detailInfo;
        console.log(result);
        wx.setStorageSync('address', result)
      },
    });
  },

  // 商品的选中优化事件
  handleItemChange(e) {
    //获取被修改的商品id
    const goods_id = e.currentTarget.dataset.id;
    // 获取购物车数组
    let {
      cart
    } = this.data;
    //找到购物车数组中要被修改的商品对象
    let index = cart.findIndex(v => v.goods_id === goods_id);
    // 修改商品对象的选中状态，取反
    cart[index].checked = !cart[index].checked;
    // 将此时的数据更新到data和缓存中，并重新计算总价格和总数量,通过调用封装的函数setCart实现
    this.setCart(cart);
  },
  //商品的全选功能
  handleItemAllChecked(e) {
    // 1获取data中的数据
    let {
      cart,
      allChecked
    } = this.data;
    // 2修改值
    allChecked = !allChecked;
    // 3循环修改cart数组中的商品选中状态
    cart.forEach(v => v.checked = allChecked);
    // 4把修改后的值填充回data或者缓存中,通过调用封装好的函数
    this.setCart(cart);
  },
  //商品数量加减功能
  handleItemNumEdit(e) {

    // 1获取传递过来的参 数
    const {
      operation,
      id
    } = e.currentTarget.dataset;
    // 2获取购物车数组
    let {
      cart
    } = this.data;
    //3找到需要修改的商品的索引
    const index = cart.findIndex(v => v.goods_id === id);
    //判定商品数量小于1时，是否要被删除
    if (cart[index].num === 1 && operation === -1) {
      wx.showModal({
        title: '提示',
        content: '您是否要删除该商品?',
        success: (res) => {
          if (res.confirm) {
            cart.splice(index, 1);
            this.setCart(cart);
          } else if (res.cancel) {
            console.log('用户点击取消');
          }
        }
      })
    } else {
      // 4进行修改数量
      cart[index].num += operation;
      // 5设置回缓存和data中
      this.setCart(cart);
    }
  },

  // 商品页面的结算功能
  handlePay(e) {
    //1. 判断收货地址和是否有商品数量
    const {
      address,
      totalNum
    } = this.data;
    if (!address.userName) {
      wx.showToast({
        title: '请添加收货地址',
        icon: 'error',
        duration: 2000,
      })
      return;
    }
    if (totalNum===0) {
      wx.showToast({
        title: '请添加商品',
        icon: 'error',
        duration: 2000
      })
      return;
    }
    // 跳转到微信支付页面
    wx.navigateTo({
      url: '/pages/pay/index',
    })
  },
  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    //声明一个变量，购物车数组
    cart: [],
    // 声明一个变量，购物车内容是否全选
    allChecked: false,
    // 定义商品总价格和总数量
    totalPrice: 0,
    totalNum: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //获取缓存中的购物车数据
    const cart = wx.getStorageSync('cart')
    // 计算是否全选购物车商品
    // every数组方法会遍历会接收个回调函数 那么每一个回调函数都返回true那么every方 法的返回值为true
    // 只要有一个回调函数返回了false 那么不再循环执行，直接返回false
    // 假如是空数组，那么every返回也是true,所以使用三元表达式
    const allChecked = cart.length ? cart.every(v => v.checked) : false;
    //获取缓存中的收货地址信息
    const address = wx.getStorageSync('address');
    //  声明总价格和总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
      if (v.checked) {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
      }
    })
    //给data赋值
    this.setData({
      address,
      cart: cart,
      allChecked,
      totalPrice,
      totalNum
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
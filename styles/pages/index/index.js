//引入用来发送请求的方法，优化后的
import {
  request
} from "../../request/index.js"
Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 轮播图数组
    swiperList: [],
    //导航数组
    cateList: [],
    //楼层数组
    floorList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 调用获取轮播图数据方法
    this.getSwiperList(),
      // 调用获取分类导航数据方法
      this.getCateList(),
      //调用获取楼层数据的方法
      this.getFloorList()
  },
  // 获取轮播图数据方法
   getSwiperList() {
    // 开始发送异步请求，获取轮播图数据,优化的手段可以通过es6的技术promise解决
    request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata"
    }).then(result => {
      this.setData({
        swiperList: result.data.message
      });
    })
  },
  // 获取分类导航数据
  getCateList() {
    // 开始发送异步请求，获取轮播图数据,优化的手段可以通过es6的技术promise解决
    request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/home/catitems"
    }).then(result => {
      this.setData({
        cateList: result.data.message
      })
    })
  },
  // 获取楼层数据
  getFloorList() {
    // 开始发送异步请求，获取轮播图数据,优化的手段可以通过es6的技术promise解决
    request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/home/floordata"
    }).then(result => {
      this.setData({
        floorList: result.data.message
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
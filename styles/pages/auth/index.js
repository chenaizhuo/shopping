// pages/auth/index.js
// 引入封装好的请求函数
import {
  request
} from "../../request/index.js"
Page({
  //获取用户信息
  async handleGetUserInfo(e) {
    // 打印下点击后获得的事件e
    // console.log(e);
    // 获取事件e中的如下属性。
    const {
      encryptedData,
      rawData,
      iv,
      signature
    } = e.detail;
    //获取小程序登录成功后的code值
    wx.login({
      timeout: 10000,
      success: (result) => {
        // 查看得到了code值
        // console.log(result);
        const code = result.code;
        wx.setStorageSync('code', code)
      },
      fail: () => {},
      complete: () => {}
    })
    const code = wx.getStorageSync('code');
    const loginParams = {
      encryptedData,
      rawData,
      iv,
      signature,
      code
    };
    //发送请求，获取用户的token值
    const token = await request({
      url: "https://api-hmugo-web.itheima.net/api/public/v1/users/wxlogin",
      data: this.loginParams,
      method: "post"
    });
    //把token存入到缓存中，同时跳转回上一个页面(这里我们使用了一个自定义的token，因为我们不是企业微信账户)
    wx.setStorageSync('token', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo');
    // 返回上一页
    wx.navigateBack({
      delta: 1
    })
    
  }
})